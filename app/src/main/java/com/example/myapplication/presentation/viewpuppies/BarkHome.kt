package com.example.myapplication.presentation.viewpuppies

import android.graphics.Paint
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.unit.dp
import com.example.myapplication.data.data_source.DataProvider
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import com.example.myapplication.domain.model.Puppy
import com.example.myapplication.ui.theme.noromalrobofonts


@Composable
fun BarkHomeContent() {

    // the remeber function in Composable function stores the  current state of the variable
    // in our case its puppies variable if the puppyList changes it will maintain the current state.
    val puppies = remember { DataProvider.puppyList }

    //This was the recylerview that we as an android developer are quite familar with it.
    LazyColumn(
        // we are providing space by using padding
        contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp)
    ) {
        // the items function - that takes puppies item as our first params
        // itemContent - that take our list item and populate with each item in  list
        items(
            items = puppies,
            itemContent = {
                PuppyListItem(puppy = it)
            })
    }
}

@Composable
fun PuppyListItem(puppy: Puppy) {

    Card(modifier = Modifier
        .padding(horizontal = 8.dp, vertical = 8.dp)
        .fillMaxWidth(),
        elevation = 2.dp,
        backgroundColor = Color.White,
        shape = RoundedCornerShape(corner = CornerSize(16.dp))
    )
    {
        //Row - represent row in a list
        Row {
            //Inside row create coloumn of two text that take puppy title and view details
            // as the second text
            PuppyImage(puppy)
            Column (modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
                .align(Alignment.CenterVertically))
            {
                Text(text = puppy.title, fontFamily = noromalrobofonts)
                Text(text = "View Details", fontFamily = noromalrobofonts)
            }
        }
    }
}

@Composable
private fun PuppyImage(puppy: Puppy) {
    Image(
        painter = painterResource(id = puppy.puppyImageId),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .padding(8.dp)
            .size(84.dp)
            .clip(RoundedCornerShape(corner = CornerSize(16.dp)))
    )
}