package com.example.myapplication.domain.model

//Model data class which holds datas of puppy
data class Puppy(val id:Int,
val title:String,
val sex:String,
val age:Int,
val description:String,
val puppyImageId:Int)
