package com.example.myapplication.data.data_source

import com.example.myapplication.R
import com.example.myapplication.domain.model.Puppy

//For Providing dummy data to recylerview
object DataProvider {
    val puppyList = listOf(Puppy(id=1,title = "Puppy 1",sex = "Male",age = 14,description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",puppyImageId = R.drawable.dummypuppy),
        Puppy(id=2,title = "Puppy 2",sex = "Male",age = 14,description = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",puppyImageId = R.drawable.dummytwo))
}